package com.franca.api_test;

import java.util.ArrayList;
import org.junit.Test;
import com.franca.utils.QueryParamsDTO;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Assert;
import static com.franca.utils.Config.debug;
import com.franca.utils.HttpTestApi;

public class FilmsApiTest {
	final String ID_DO_FILME = "tt1285016 ";
	// final String API_KEY = "52ec71bf";

	ArrayList<QueryParamsDTO> queryParams = new ArrayList<QueryParamsDTO>();
	QueryParamsDTO apikey = new QueryParamsDTO();
	QueryParamsDTO idFilme = new QueryParamsDTO();

	@Test
	public void deveBuscarFilmePorIdCorretamente() {
		apikey.setChave("apikey");
		apikey.setValor("52ec71bf");

		idFilme.setChave("i");
		idFilme.setValor("tt1285016");

		queryParams.add(apikey);
		queryParams.add(idFilme);
		// O resource est� indo com uma string vazia, pois normalmente eu crio
		// uma
		// vari�vel com a url base e no teste eu insiro o recurso a ser testado.
		// Neste caso a url n possui recurso
		Response response = HttpTestApi.doGetRequest("", queryParams);
		JsonPath jsonPath = response.jsonPath();
		/**
		 * Eu crio dentro da classe de config uma vari�vel debug que quando est�
		 * true exibir� no console todos os resultados das chamadas caso queira
		 * examinar, caso queira somente executar os testes sem log, apenas
		 * setar como false.
		 */
		if (debug)
			System.out.println("get film: " + jsonPath.get("$"));

		// Recupera valores a serem testados
		String idiomas = jsonPath.getString("Language");
		String anoFilme = jsonPath.getString("Year");
		String tituloFilme = jsonPath.getString("Title");
		System.out.println(tituloFilme);

		// Asser��es
		Assert.assertEquals(tituloFilme, "The Social Network");
		Assert.assertEquals(anoFilme, "2010");
		Assert.assertEquals(idiomas, "English, French");

	}

	@Test
	public void deveReceberErroAoBuscarFilmeInexistente() {
		apikey.setChave("apikey");
		apikey.setValor("52ec71bf");

		idFilme.setChave("i");
		idFilme.setValor("tt1285016ERRADO");

		queryParams.add(apikey);
		queryParams.add(idFilme);

		Response response = HttpTestApi.doGetRequest("", queryParams);
		JsonPath jsonPath = response.jsonPath();

		if (debug)
			System.out.println("response error: " + jsonPath.get("$"));
		// Asser��es
		Assert.assertEquals(jsonPath.getBoolean("Response"), false);
		Assert.assertEquals(jsonPath.get("Error"), "Incorrect IMDb ID.");
	}
}
