package com.franca.utils;

import java.util.ArrayList;

import com.franca.utils.Config;
import com.franca.utils.QueryParamsDTO;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static com.franca.utils.Config.enviroment;

public class HttpTestApi {

	public static Response doGetRequest(String resource, ArrayList<QueryParamsDTO> params) {
		RestAssured.baseURI = enviroment;
		RequestSpecification httpRequest = RestAssured.given();

		for (int i = 0; i < params.size(); i++) {

			httpRequest.queryParam(params.get(i).getChave(), params.get(i).getValor());
		}
		Response response = httpRequest.get(resource);

		return response;

	}
}
