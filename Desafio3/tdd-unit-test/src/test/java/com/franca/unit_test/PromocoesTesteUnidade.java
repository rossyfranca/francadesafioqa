package com.franca.unit_test;

import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.franca.services.Promocoes;

public class PromocoesTesteUnidade {
	ArrayList<Integer> carrinho = new ArrayList<Integer>();

	@Before
	public void before() {
		for (int i = 1; i < 7; i++) {
			carrinho.add(i);
		}
	}

	@Test
	public void deveCalcularDescontoCorretamente() {

		Promocoes promocoes = new Promocoes();
		int desconto = promocoes.calculaDesconto(carrinho);

		Assert.assertEquals(35, desconto);
		// System.out.println(carrinho);

	}

}
