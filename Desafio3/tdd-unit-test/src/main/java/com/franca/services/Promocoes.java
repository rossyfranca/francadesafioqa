package com.franca.services;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONObject;

public class Promocoes {
	JSONObject filme = new JSONObject();
	JSONObject filme2 = new JSONObject();
	JSONObject filme3 = new JSONObject();
	JSONObject filme4 = new JSONObject();
	JSONObject filme5 = new JSONObject();
	JSONObject filme6 = new JSONObject();
	ArrayList<JSONObject> filmesBanco = new ArrayList<JSONObject>();
	ArrayList<JSONObject> banco = new ArrayList<JSONObject>();
	double totalCompra = 0;
	int totalDescontoFidelidade = 0;
	boolean contemFilmeDeAcao = false;

	public int calculaDesconto(ArrayList<Integer> carrinho) {
		double compra = calculaTotalCompra(carrinho);

		if (compra <= 200 && compra >= 100)
			totalDescontoFidelidade = 10;
		if (compra < 300 && compra > 200)
			totalDescontoFidelidade = 20;
		if (compra <= 400 && compra > 300)
			totalDescontoFidelidade = 25;
		if (compra > 400)
			totalDescontoFidelidade = 30;

		if (contemFilmeDeAcao)
			totalDescontoFidelidade += 5;

		return totalDescontoFidelidade;
	}

	@SuppressWarnings("unchecked")
	public double calculaTotalCompra(ArrayList<Integer> carrinho) {
		banco = new ArrayList<JSONObject>();
		banco = retornaListaMockFilmes();
		carrinho.forEach((filme) -> {
			banco.forEach((f) -> {
				if (filme == f.get("id")) {
					if (f.get("genero") == "acao")
						contemFilmeDeAcao = true;
					double valorUnidade = (double) f.get("valor");
					totalCompra += valorUnidade;
					System.out.println(totalCompra);
				} else
					return;
			});
		});
		return totalCompra;

	}

	@SuppressWarnings("unchecked")
	public ArrayList<JSONObject> retornaListaMockFilmes() {
		// Mock de filmes no banco
		filme.put("id", 1);
		filme.put("valor", 45.00);
		filme.put("genero", "fantasia");
		filmesBanco.add(filme);
		filme2.put("id", 2);
		filme2.put("valor", 55.00);
		filme2.put("genero", "comedia");
		filmesBanco.add(filme2);
		filme3.put("id", 3);
		filme3.put("valor", 100.00);
		filme3.put("genero", "acao");
		filmesBanco.add(filme3);
		filme4.put("id", 4);
		filme4.put("valor", 55.00);
		filme4.put("genero", "acao");
		filmesBanco.add(filme4);
		filme5.put("id", 5);
		filme5.put("valor", 100.00);
		filme5.put("genero", "drama");
		filmesBanco.add(filme5);
		filme6.put("id", 6);
		filme6.put("valor", 200.00);
		filme6.put("genero", "animacao");
		filmesBanco.add(filme6);

		return filmesBanco;
	}
}
