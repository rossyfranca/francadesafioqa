package com.franca.pages;

import static com.franca.utils.Config.headless;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageDefault {

	WebDriver driver;
	WebDriverWait wait;

	public PageDefault(WebDriver driver) {
		this.driver = driver;
	}

	public PageDefault() {
		ChromeOptions options = new ChromeOptions();
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 1);
		options.setExperimentalOption("prefs", prefs);
		// utils.Config
		if (headless)
			options.addArguments("--headless");
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		this.driver = new ChromeDriver(options);
		wait = new WebDriverWait(this.driver, 10);
	}

	public void navigateTo(String url) {
		driver.navigate().to(url);
		driver.manage().window().maximize();
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void closeNavigator() {
		getDriver().close();
	}

	public void waitFor(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickById(String cssId) {
		getDriver().findElement(By.id(cssId)).click();
	}

	public void click(String cssSelector) {
		getDriver().findElement(By.cssSelector(cssSelector)).click();
	}

	public void type(String cssSelector, String value) {
		getDriver().findElement(By.cssSelector(cssSelector)).sendKeys(value);
	}

	public void typeById(String cssId, String value) {
		getDriver().findElement(By.id(cssId)).sendKeys(value);
	}

	public void selecDropDown(String cssId, int index) {
		WebElement mySelectElement = driver.findElement(By.id(cssId));
		Select dropdown = new Select(mySelectElement);

		dropdown.selectByIndex(index);
	}

	public List<WebElement> getListElementsById(String cssId) {
		List<WebElement> list = getDriver().findElements(By.id(cssId));
		return list;
	}

	public List<WebElement> getListElements(String cssSelector) {
		List<WebElement> list = getDriver().findElements(By.cssSelector(cssSelector));
		return list;
	}
}
