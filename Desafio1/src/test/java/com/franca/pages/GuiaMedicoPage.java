package com.franca.pages;

import org.openqa.selenium.WebDriver;

public class GuiaMedicoPage extends PageDefault {
	public GuiaMedicoPage(WebDriver driver) {
		super(driver);
	}

	// O que você procura?
	public String oqueVoceProcura = "campo_pesquisa";
	public String pesquisar = "btn_pesquisar";
	public String estado = "#react-select-2-input";
	public String cidade = "#react-select-3-input";
	public String radioUnimed = "input[type='radio']";
	public String continuar = "button.btn.btn-success";
	public String resultadoPesquisa = "div[id^='resultado']";
	public String enderecoResultado = "span#txt_endereco > p";
	public String paginacao = "a[href='#lista_por']";
}
