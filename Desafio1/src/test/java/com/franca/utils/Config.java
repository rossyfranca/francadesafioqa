package com.franca.utils;

public class Config {

	public static boolean headless = false;
	private String url_base = "https://www.unimed.coop.br/";

	public String getUrl() {
		return url_base;
	}
}
