#language: pt
Funcionalidade: Pesquisar por estado
  Como usuario,
  eu quero realizar uma pesquisa detalhada por estado
  para que o usu�rio possa filtrar a pesquisa

  Cenario: Deve realizar uma pesquisa sem erros
    Quando Acessar o guia medico
    E buscar por medico
    E selecionar o estado
    Entao a pesquisa deve fialtrar pelo estado inserido
